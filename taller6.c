#include <stdio.h>
#include <sys/wait.h>
#include <unistd.h>
#include <sys/types.h>
#include <string.h>

char buffer1[100];

int main(int argc, char** argv){
        memset(buffer1,0,100);
        for(int i=1;i<argc;i++){
                snprintf(buffer1,100,"%d.png",i);
                pid_t nid=fork();
                if(nid==0){
                        execl("procesador_png","procesador_png",argv[i],buffer1,NULL);
                        printf("Error execl\n");
                }
                memset(buffer1,0,100);
        }
	for(int i=1; i<argc;i++){
		memset(buffer1,0,100);
		int status;
                wait(&status);
		snprintf(buffer1,100,"%d.png",i);
                printf("%s (archivo BW %s): status %d",argv[i],buffer1,WEXITSTATUS(status));
	
	
	}
        return 0;
}
