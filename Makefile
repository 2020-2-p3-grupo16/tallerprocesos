taller6: taller6.o
	gcc taller6.o -o taller6

taller6.o: taller6.c
	gcc -Wall -c taller6.c -o taller6.o

clean:
	rm taller6 taller6.o
